#!/bin/bash

CALENDAR="calendar.txt"
CALENDAR_DATES="calendar_dates.txt"

file_exists(){
    local f="$1"
    [[ -f "$f" ]] && return 0 || return 1
}

file_does_not_exist(){
    local f="$1"
    [[ -f "$f" ]] && return 1 || return 0
}

if ( file_exists $CALENDAR )
then
    echo "File calendar.txt already present"
    exit 0
fi

if ( file_does_not_exist $CALENDAR_DATES )
then
    echo "File calendar_dates.txt not found"
    exit 1
fi

echo "service_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,start_date,end_date" > $CALENDAR

while read line;
do
    IFS=','
    read -r serviceid remainder <<< "$line"
    IFS=''
    pattern=^$(eval echo $serviceid),
    if [[ $(grep -c $pattern $CALENDAR) -eq 0 ]]
    then
	echo "$serviceid,0,0,0,0,0,0,0,19000101,19000102" >> $CALENDAR
    fi
done <$CALENDAR_DATES
