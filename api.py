from flask import Flask, jsonify
import dbquery
import json
from cachetools import cached

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello'

@app.route('/api')
def usage():
    return ('Usage: enter origin, destination, date, time (optional). '
            + 'http://.../api/origin/destination/date/time'
            + " or "
            + 'http://.../api/origin/destination/date')

# wildcards "*": asterisks are translated in the MySQL % wildcard
# single quotes "'": escaped
def _translate_place_query(place):
    wildcardOK = place.replace("*","%")
    return wildcardOK.replace("'", "\\\'")

@app.route('/api/<origin>/<destination>/<date>/<time>')
@app.route('/api/<origin>/<destination>/<date>')
@cached(cache={})
def get_json(origin,destination,date,time="00:00:00"):
    return dbquery.getJson(_translate_place_query(origin),
                           _translate_place_query(destination),
                           date,
                           time)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
