DROP DATABASE IF EXISTS emtmalaga;

CREATE DATABASE emtmalaga
	DEFAULT CHARACTER SET utf8
	DEFAULT COLLATE utf8_general_ci;

USE emtmalaga


DROP TABLE IF EXISTS shapes;
-- shape_id,shape_pt_lat,shape_pt_lon,shape_pt_sequence
CREATE TABLE `shapes` (
	shape_id VARCHAR(255) NOT NULL,
	shape_pt_lat DECIMAL(8,6),
	shape_pt_lon DECIMAL(8,6),
	shape_pt_sequence VARCHAR(255),
	CONSTRAINT PK_shapes PRIMARY KEY (shape_id, shape_pt_sequence) -- TODO check
);

-- https://developers.google.com/transit/gtfs/reference/calendar-file
DROP TABLE IF EXISTS calendar;
-- service_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,start_date,end_date
CREATE TABLE `calendar` (
    	service_id VARCHAR(255) NOT NULL PRIMARY KEY,
	monday TINYINT(1),
	tuesday TINYINT(1),
	wednesday TINYINT(1),
	thursday TINYINT(1),
	friday TINYINT(1),
	saturday TINYINT(1),
	sunday TINYINT(1),
	start_date VARCHAR(8),	
	end_date VARCHAR(8)
);

-- https://developers.google.com/transit/gtfs/reference/calendar_dates-file
DROP TABLE IF EXISTS calendar_dates;
-- service_id,date,exception_type
CREATE TABLE `calendar_dates` (
    service_id VARCHAR(255),
    `date` VARCHAR(8),
    exception_type INT(2),
    CONSTRAINT PK_calendar_dates PRIMARY KEY (service_id, `date`), -- TODO check
    FOREIGN KEY (service_id) REFERENCES calendar(service_id),
    KEY `exception_type` (exception_type)    
);

DROP TABLE IF EXISTS routes;
-- route_id,route_short_name,route_long_name,route_type,route_url

CREATE TABLE `routes` (
    	route_id VARCHAR(255) NOT NULL PRIMARY KEY,
	route_short_name VARCHAR(50),
	route_long_name VARCHAR(255),
	route_type INT(2),
	route_url VARCHAR(255),
	KEY `route_type` (route_type)
);



DROP TABLE IF EXISTS trips;
-- route_id,service_id,trip_id,trip_headsign,direction_id,shape_id

CREATE TABLE `trips` ( -- TODO check
	route_id VARCHAR(255),
	service_id VARCHAR(255),
	trip_id VARCHAR(255) NOT NULL PRIMARY KEY,
	trip_headsign VARCHAR(255),
	direction_id TINYINT(1),
	shape_id VARCHAR(255),
	FOREIGN KEY (service_id) REFERENCES calendar(service_id),
	-- FOREIGN KEY (shape_id) REFERENCES shapes(shape_id), -- TODO we have PRIMARY KEY (shape_id, shape_pt_sequence) in table `shapes`
	KEY `route_id` (route_id),
	KEY `service_id` (service_id),
	KEY `direction_id` (direction_id)
);


DROP TABLE IF EXISTS stops;
-- stop_id,stop_code,stop_name,stop_lat,stop_lon,stop_url
CREATE TABLE `stops` (
    	stop_id VARCHAR(255) NOT NULL PRIMARY KEY,
	stop_code VARCHAR(255),
	stop_name VARCHAR(255),
	stop_lat DECIMAL(8,6),
	stop_lon DECIMAL(8,6),
	stop_url VARCHAR(500)
);


DROP TABLE IF EXISTS stop_times;
-- trip_id,arrival_time,departure_time,stop_id,stop_sequence
CREATE TABLE `stop_times` (
    	trip_id VARCHAR(255),
	arrival_time VARCHAR(8),
	departure_time VARCHAR(8),
	stop_id VARCHAR(255),
	stop_sequence VARCHAR(255),
	FOREIGN KEY (trip_id) REFERENCES trips(trip_id),
	FOREIGN KEY (stop_id) REFERENCES stops(stop_id),
	KEY `trip_id` (trip_id),
	KEY `stop_id` (stop_id),
	KEY `stop_sequence` (stop_sequence)
);


DROP TABLE IF EXISTS frequencies;
-- trip_id,start_time,end_time,headway_secs
CREATE TABLE `frequencies` (
	trip_id VARCHAR(255),
	start_time VARCHAR(50),
	end_time VARCHAR(50),
	headway_secs VARCHAR(50),
	CONSTRAINT PK_frequencies PRIMARY KEY (trip_id, start_time, end_time), -- TODO check
	FOREIGN KEY (trip_id) REFERENCES trips(trip_id)
);



LOAD DATA LOCAL INFILE 'shapes.txt' INTO TABLE shapes FIELDS TERMINATED BY ',' IGNORE 1 LINES;

LOAD DATA LOCAL INFILE 'calendar.txt' INTO TABLE calendar FIELDS TERMINATED BY ',' IGNORE 1 LINES;

LOAD DATA LOCAL INFILE 'calendar_dates.txt' INTO TABLE calendar_dates FIELDS TERMINATED BY ',' IGNORE 1 LINES;

LOAD DATA LOCAL INFILE 'routes.txt' INTO TABLE routes FIELDS TERMINATED BY ',' IGNORE 1 LINES;

LOAD DATA LOCAL INFILE 'stops.txt' INTO TABLE stops FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' IGNORE 1 LINES;

LOAD DATA LOCAL INFILE 'trips.txt' INTO TABLE trips FIELDS TERMINATED BY ','  OPTIONALLY ENCLOSED BY '"' IGNORE 1 LINES;

LOAD DATA LOCAL INFILE 'frequencies.txt' INTO TABLE frequencies FIELDS TERMINATED BY ',' IGNORE 1 LINES;

LOAD DATA LOCAL INFILE 'stop_times.txt' INTO TABLE stop_times FIELDS TERMINATED BY ',' IGNORE 1 LINES;
