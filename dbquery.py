#!/usr/bin/python env
# -*- coding: utf-8 -*-

import MySQLdb as sql
import credentials as cr
import sys

RESULTS_LIMIT = 10


def makeQuery(origin, destination, date, time, limit):
    return ("SELECT DISTINCT route_id, DESTINATION.trip_headsign, departure_time, ORIGIN.stop_name, arrival_time, DESTINATION.stop_name FROM (select route_id, trip_id, departure_time, stop_name from routes NATURAL JOIN trips NATURAL JOIN stop_times NATURAL JOIN stops where (stop_id in (select stop_id from stops where stop_name like '" + origin + "')) AND departure_time >= '" +time+ "') AS ORIGIN INNER JOIN (select route_id, trip_id, arrival_time, trip_headsign, stop_name from routes NATURAL JOIN trips NATURAL JOIN stop_times NATURAL JOIN stops where (stop_id in (select stop_id from stops where stop_name like '" + destination + "'))) AS DESTINATION USING (route_id, trip_id) WHERE departure_time < arrival_time AND trip_id in (select trip_id from ((select T.trip_id from trips T, calendar C where T.service_id = C.service_id and ((WEEKDAY('"+date+"') = 0 and C.monday = 1) or (WEEKDAY('"+date+"') = 1 and C.tuesday = 1) or (WEEKDAY('"+date+"') = 2 and C.wednesday = 1) or (WEEKDAY('"+date+"') = 3 and C.thursday = 1) or (WEEKDAY('"+date+"') = 4 and C.friday = 1) or (WEEKDAY('"+date+"') = 5 and C.saturday = 1) or (WEEKDAY('"+date+"') = 6 and C.friday = 1))) union (select T.trip_id from trips T, calendar_dates CD where T.service_id = CD.service_id and CD.date = '" + date + "' and CD.exception_type = 1)) as SHOULDBYGOING where trip_id not in (select T.trip_id from trips T, calendar_dates CD where T.service_id = CD.service_id and CD.date = '" + date + "' and CD.exception_type = 0)) ORDER BY departure_time LIMIT " + str(limit))


def getJson(origin, destination, date, time):
    cnx = sql.connect(user=cr.USERNAME,
                      passwd=cr.PASSWORD,
                      host=cr.HOST,
                      db=cr.DATABASE,
                      charset='utf8')                      

    cur = cnx.cursor()
    cur.execute(makeQuery(origin, destination, date, time, RESULTS_LIMIT))
    rows = cur.fetchall()
    cur.close
    cnx.close()

    result = ""
    result += "{"
    # query info
    result += ("\"query\": {{\"origin\": \"{}\", " +
               "\"destination\": \"{}\", " +
               "\"date\": \"{}\", " +
               "\"time\": \"{}\"}}, ").format(origin.replace("\\", ""),
                                              destination.replace("\\", ""),
                                              date,
                                              time)
    # results from the database
    result += "\"results\": ["
    for r in range(0,len(rows)):
        result += "{"
        result += ("\"route\": \"{}\", " +
               "\"headsign\": \"{}\", " +
               "\"departure_time\": \"{}\", " +
               "\"departure_stop_name\": \"{}\", " +
               "\"arrival_time\": \"{}\", " +                   
               "\"arrival_stop_name\": \"{}\"").format(
                   rows[r][0].encode('utf8'),
                   rows[r][1].encode('utf8'),
                   rows[r][2].encode('utf8'),
                   rows[r][3].encode('utf8'),
                   rows[r][4].encode('utf8'),
                   rows[r][5].encode('utf8'))
        if r == len(rows) - 1:
            result += "}"
        else:
            result += "},"            
    result += "]"
    result += "}"

    return result


if __name__ == "__main__":    
    print getJson(sys.argv[1], sys.argv[2],
                  sys.argv[3], sys.argv[4])
    # example: call with    
    # python2 dbquery.py "%Trento%" "%Rome%" "20170424" "00:00:00"
