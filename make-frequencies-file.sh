#!/bin/bash

FREQUENCIES="frequencies.txt"

file_exists(){
    local f="$1"
    [[ -f "$f" ]] && return 0 || return 1
}

if ( file_exists $FREQUENCIES )
then
    echo "File frequencies.txt already present"
    exit 0
fi

echo "trip_id,start_time,end_time,headway_secs" > $FREQUENCIES
